TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    orig/abo1.c \
    orig/abo2.c \
    orig/abo3.c \
    orig/abo4.c \
    orig/abo5.c \
    orig/abo6.c \
    orig/abo7.c \
    orig/abo8.c \
    orig/abo9.c \
    orig/abo10.c \
    orig/e1.c \
    orig/e2.c \
    orig/e3.c \
    orig/e4.c \
    orig/e5.c \
    orig/fs1.c \
    orig/fs2.c \
    orig/fs3.c \
    orig/fs4.c \
    orig/fs5.c \
    orig/n1.c \
    orig/n2.c \
    orig/n3.c \
    orig/n4.c \
    orig/n5.c \
    orig/s1.c \
    orig/s2.c \
    orig/s3.c \
    orig/s4.c \
    orig/sg1.c \
    orig/sg2.c \
    orig/sg3.c \
    orig/sg4.c \
    orig/sg5.c \
    orig/sg6.c \
    orig/stack1.c \
    orig/stack2.c \
    orig/stack3.c \
    orig/stack4.c \
    orig/stack5.c

